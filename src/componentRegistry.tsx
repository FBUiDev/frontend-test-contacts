import ContactsComponent from "./js/view/contact/contacts.component";
import ViewContactsComponent from "./js/view/contact/viewContacts.component";

export default {
    ContactsComponent: 'ContactsComponent',
    ViewContactsComponent: 'ViewContactsComponent',
};