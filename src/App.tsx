import React, { Component } from "react";
// import { connect, Provider } from 'react-redux';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";
import ContactsComponent from "./js/view/contact/contacts.component";
import ViewContactsComponent from "./js/view/contact/viewContacts.component";

class App extends Component{
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Contacts</h1>
        </header>
        <div>
            <Router>
              <Route path="/" exact component={ContactsComponent}  />
              <Route path="/contact/:id" component={ViewContactsComponent} />
            </Router>
        </div>
      </div>
    );
  }
}

export default App;
