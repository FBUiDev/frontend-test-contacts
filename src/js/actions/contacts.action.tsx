const apiURL = "http://localhost:3001/contacts/";

export function newContact(data) {
    return fetch(apiURL, {
        method: "POST",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

export function updateContact(data) {
    return fetch(apiURL + data.id, {
        method: "PUT",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
}

export function deleteContact(data) {
    return fetch(apiURL + data.id, {
        method: "delete",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
}