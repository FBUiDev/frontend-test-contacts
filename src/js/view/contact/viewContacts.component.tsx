import React, { Component } from 'react';
import PropTypes, {array, object, number} from 'prop-types';
import _filter from 'lodash/filter';
import {
    Card,
    CardContent,
    CardHeader,
    Typography
} from '@material-ui/core';
import {AppProps, CurrentState} from "./contacts.component";

export default class ViewContactsComponent extends Component<AppProps, CurrentState>{
    constructor(props: AppProps, state: CurrentState) {
        super(props);
        console.log(props);
    }

    renderListDetail() {
        const {id: string} = this.props.match.params;
        const { state } = this.props.history.location;
        const v = {id: string};
        return (
            <Card>
                <CardContent>
                <Typography style={{fontSize: 16, fontWeight: 600}} color="textPrimary" gutterBottom>Name: {state.name}</Typography>
                <Typography color="textSecondary">id: {state.id}</Typography>
                <Typography color="textSecondary">CreatedDate: {state.createdDate}</Typography>
                <Typography color="textSecondary">ModifiedDate: {state.modifiedDate}</Typography>
                </CardContent>
            </Card>
        );
    }

    render() {
        const {id: string} = this.props.match.params;
        return this.renderListDetail();
    }
}