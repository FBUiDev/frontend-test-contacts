import React, {Component } from 'react';
import { Link } from "react-router-dom";
import { 
    Button, 
    Fab, 
    List,
    ListItem,
} from '@material-ui/core';
import _map from 'lodash/map';
import {
    newContact, 
    updateContact, 
    deleteContact
} from '../../actions/contacts.action';

export type AppProps = {
    history: {
        location: {
            state: {
                id: number[];
                name: string;
                createdDate: string;
                modifiedDate: string;
            }
        }
    };
    match: {
        params: {
            id: string[];
        }
    };
};

export type CurrentState = {
    apiURL: string,
    isNew: boolean,
    isSubmitted: boolean,
    name:string,
    mode: string,
    selectedContact: {
        id: number,
        createdDate: string,
        modifiedDate: string,
        name: string,
    },
    list: {
        id: number,
        createdDate: string,
        modifiedDate: string,
        name: string,
    }[];
};

export default class ContactsComponent extends Component<AppProps, CurrentState>{
    constructor(props: AppProps) {
        super(props);
        this.state = {
            apiURL: "http://localhost:3001/contacts/",
            isNew: false,
            isSubmitted: false,
            name: '',
            mode: 'add',
            selectedContact: null,
            list: [],
        };
        
        this.handleAddContact = this.handleAddContact.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount(): void{
        this.fectchList();
    }

    componentDidUpdate(prevProp, prevState): void{
        if (prevState.fetched) {
           console.log('on update of list', this.state.list);
        }
    }

    fectchList() {
        return fetch(this.state.apiURL)
            .then(res => res.json())
            .then(data => this.setState({ list: data }))
            .catch((error) => {
                console.log(error);
            });
    }

    handleAddContact() {
        this.setState({
            isNew: true,
            name: '',
        });
    }

    handleEditContact(event, item) {
        const{id, createdDate, name} = item;
        this.setState({
            isNew: true,
            name,
            mode: 'edit',
            selectedContact: {
                id,
                createdDate,
                modifiedDate: '',
                name,
            }
        });
    }


    handleDeleteContact(event, item) {
        deleteContact(item).then(() => {
            this.fectchList();
        }).catch((error) => {
            console.log(error);
        });
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState<never>({
            [name]: value
        });
    }

    handleClose(e) {
        this.setState({
            isNew: false
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        const { name, list } = this.state;
        const arrLength = list.length;
        this.setState({ isSubmitted: true });
		if (!(name)) {
		    return;
        }

        this.setState({isNew: false}, () => {
            newContact({
                id: Math.floor(Math.random() * 1000000000),
                createdDate: new Date(),
                name
            }).then(() => {
                this.fectchList();
            }).catch((error) => {
                console.log(error);
            });
        });
    }

    handleUpdate(e) {
        e.preventDefault();
        const { id, createdDate } = this.state.selectedContact;
        const { name } = this.state;
        this.setState({ isSubmitted: true });
		if (!(name)) {
		    return;
		}
        this.setState({isNew: false}, () => {
            updateContact({
                id,
                createdDate,
                modifiedDate: new Date(),
                name
            }).then(() => {
                this.fectchList();
            }).catch((error) => {
                console.log(error);
            });
        });
    }

    renderList() {
        const { list } = this.state;
        const listitem =  _map(list, (item, index) =>
            <List key={index} style={{background: '#dddddd', width: 600, color: '#000000'}} aria-label="contacts">
                    <ListItem alignItems="flex-start" >
                <Link style={{textDecoration: 'none', color: '#000000', width: 415}} to={{
                    pathname: `/contact/${item.id}`,
                    state: item,
                }}>{item.name}</Link>
                <div className={'button-group'}>
                    <Button variant="contained" color="secondary" 
                    onClick={event => this.handleEditContact(event, item)}>edit</Button>
                    <Button variant="contained" color="inherit" 
                    onClick={event => this.handleDeleteContact(event, item)}>delete</Button>
                </div>
                </ListItem>
            </List>
        );
        return listitem;
    }

    renderForm() {
        if (!this.state.isNew) {
           return; 
        }
        return (
            <form onSubmit={this.state.mode === 'edit' ? this.handleUpdate : this.handleSubmit} style={{width: 600, height: 175, background: '#fff', marginTop: -62,  zIndex: 100,
            position: 'relative'}}>
                 <div className="form-group">
                     <label htmlFor="my-input">Name</label>
                    <input id="name" name="name" value={this.state.name} onChange={this.handleChange} />
                 </div>
                

                <div className="form-group">
					<button>Submit</button>
                    <button type="button" onClick={this.handleClose}>Cancel</button>
                </div>
            </form>
        )
    }

    render() {
        return(
            <div className="contact-list">
                <div className={'button'}>
                <Fab color="primary" aria-label="add" onClick={this.handleAddContact}>
                    +
                </Fab>
                </div>
                {this.renderForm()}
                {this.renderList()}
            </div>
        )
    }
}